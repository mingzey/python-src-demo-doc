# Python 人工智能零基础入门到入土

## 介绍
通过本教程，从零基础开始学习人工智能。  
本教程涉猎范围广泛，从基础定义、数学基础到代码编写多角度进行教学。  
如果您是绝对的零基础，请仔细阅读每一章内容。  

想要贡献？
- 在本仓库 Fork 并 Pull Request 即可
- 加入团队联系QQ：1552904342

## 目录
+ [(一) Python 能做些什么？](https://gitee.com/mingzey/python-src-demo-doc/blob/master/book/b1.md)
+ (二) 灵魂拷问：什么是代码？
+ [(三) Python 环境的安装](https://gitee.com/mingzey/python-src-demo-doc/blob/master/book/b210917.md)
+ (四) 熟悉 Python Sheel
+ (五) 开始挖坟吧：第一个工程 Hello World
+ ...

+ 准备入土了：人工智能的基本概念
+ 线性代数在人工智能中的应用
+ 仿生学 之 麦卡洛克-皮特斯模型(MP模型)
+ 阶跃函数
+ 分类问题
+ 第一个人工智障：识别数字
+ 卷积神经网络


### 源码实例
- 无

### 文档
- [Python Format](https://gitee.com/mingzey/python-src-demo-doc/blob/master/doc/format.md)
- [Python Eval](https://gitee.com/mingzey/python-src-demo-doc/blob/master/doc/eval.md)
